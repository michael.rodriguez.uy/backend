package com.fing.backend.business;

import com.fing.backend.business.interfaces.INotificationService;
import com.fing.backend.dao.mongo.ITenantRepositoryNsql;
import com.fing.backend.entity.TenantInfo;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

import com.google.firebase.messaging.TopicManagementResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@Slf4j
public class NotificationService implements INotificationService {

    @Autowired private ITenantRepositoryNsql iTenantRepository;

    /* ejemplo de notificacion personalizada
    public void notificarCreacionUser(Usuario u, String token) {
        try {
            if (u != null) {
                Firestore db = FirestoreClient.getFirestore();
                // send notificacion a server
                Notificacion dt = new Notificacion();
                dt.setUser(u.getNombreUsuario());
                dt.setCorreo(u.getCorreo());
                dt.setEdad(calcEdad(u.getBirthdate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalDate.now()));
                dt.setDescripcion("Usuario creado: " + dt.getUser() + "\n" + "Edad: " + dt.getEdad());
                db.collection("urubuyFS.creaciones").add(dt).get();
                if (true) {
                    //String topic = "CreacionUsuario";
                    Notification notfy = Notification.builder().setTitle("UruBuy")
                            .setBody(dt.getDescripcion()).build();
                    Message message = Message.builder().setNotification(notfy)
                            .putData("Edad", String.valueOf(dt.getEdad()))
                            .putData("Nombre", dt.getUser()).build();
                    //  .setTopic(topic)
                    //.setToken(token).build();
                    FirebaseMessaging.getInstance().send(message);
                    log.info("Firebase notification: " + dt.getUser() + " creado con edad: " + dt.getEdad());
                }

            } else log.error("que paso");
        } catch (Exception e) {
            log.error("Firebase error: " + e.getMessage());
        }
    } */

    // suscribo los tokens en mongo a un topic, para mandarle push noticias, promociones o cosas
    public void suscribeTenants() { // to topics
        List<TenantInfo> tenants = iTenantRepository.findAll();
        List<String> registrationTokens = new ArrayList<String>();
        for(TenantInfo s: tenants)
            registrationTokens.add(s.getToken());
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(
                    registrationTokens, "promociones");
            log.info(response.getSuccessCount() + " tokens were subscribed successfully");
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    // suscribir un cliente de un topic
    public void suscribeTokenFS(String token, String topic) {       // from topics
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(
                    Arrays.asList(token), topic);
            log.info(response.getSuccessCount() + " tokens were subscribed successfully");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    // la doc de google dice que hasta 1000 tokens se pueden suscribir. Pa no llegar a 1000 y como no se si los token que ya suscribió los vuelve a suscribir o los repite
    // tambien sirve para que el usuario configure sus subscripciones y elija que quiere, van a ver varios suscribe y unsuscribe topic
    public void unsuscribeTenants() {       // from topics; a todos los de la bd
        List<TenantInfo> tenants = iTenantRepository.findAll();
        List<String> registrationTokens = new ArrayList<String>();
        for(TenantInfo s: tenants)
            registrationTokens.add(s.getToken());
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(
                    registrationTokens, "promociones");
            log.info(response.getSuccessCount() + " tokens were unsubscribed successfully");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    // desuscribir un cliente de un topic
    public void unsuscribeTokenFS(String token, String topic) {       // from topics
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(
                    Arrays.asList(token), topic);
            log.info(response.getSuccessCount() + " tokens were unsubscribed successfully");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /* test push notify firebase */
    public void sendNotification() {
        try {
            Notification notification = Notification.builder().setTitle("UruBuy")
                    .setBody("Promociones").build();
            Message message = Message.builder().setNotification(notification)
                    .putData("Championes", "Nike AirMax")
                    .putData("Descuento", "50%")
                    .setTopic("promociones").build();

            FirebaseMessaging.getInstance().send(message);
            log.info("Firebase notification a promociones");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.fing.backend.business.interfaces;

public interface INotificationService {

    void suscribeTenants(); // todos
    void unsuscribeTenants();
    void suscribeTokenFS(String token, String topic);   // de a 1
    void unsuscribeTokenFS(String token, String topic);
    void sendNotification(); // test op

}

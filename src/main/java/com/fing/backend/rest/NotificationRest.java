package com.fing.backend.rest;

import com.fing.backend.business.interfaces.INotificationService;
import com.fing.backend.dao.mongo.ITenantRepositoryNsql;
import com.fing.backend.dto.RecipientsDTO;
import com.fing.backend.entity.TenantInfo;

import io.github.jav.exposerversdk.PushClient;
import io.github.jav.exposerversdk.PushClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import com.mongodb.client.result.UpdateResult;
import io.github.jav.exposerversdk.ExpoPushMessage;
import io.github.jav.exposerversdk.ExpoPushMessageTicketPair;
import io.github.jav.exposerversdk.ExpoPushTicket;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/notifications")
@CrossOrigin(value = "*")
@Slf4j
public class NotificationRest {

    @Autowired private ITenantRepositoryNsql iTenantRepository;
    @Autowired private MongoTemplate mongoTemplate;
    @Autowired private INotificationService notificationService;

    // firebase
    @PostMapping("/addFS")
    public ResponseEntity<Object> addTokenFirebase(@RequestBody TenantInfo tenantInfo) throws Exception {
        if (Objects.isNull(tenantInfo))
            return new ResponseEntity<>("Datos requerido.", HttpStatus.PRECONDITION_REQUIRED);
        else {
            try {
                if (manageAdd(tenantInfo))   // se fija si ya existe el token; si devuelve true lo persiste, si devuelve false significa que lo actualizó porqe ya existia un token para ese tenant
                    // guarda en mongo el con Id device o Id Usuario
                    iTenantRepository.save(tenantInfo);
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
            }
        }
    }

    private boolean manageAdd(TenantInfo tenant) {
        TenantInfo tenantInfo = iTenantRepository.findById(tenant.getIdTenant()).orElse(null);
        if (Objects.isNull(tenantInfo))
            return true;    // no existe lo persisto
        else {
            if (!tenantInfo.getToken().equals(tenant.getToken())) {
                // existe y no son iguales actualizo
                if (!tenant.getIdTenant().contains("RNExpo"))            // desuscribo el token viejo de firebase
                    notificationService.unsuscribeTokenFS(tenantInfo.getToken(), "promociones");
                Query query = new Query(Criteria.where("idTenant").is(tenant.getIdTenant()));
                Update update = new Update();
                update.set("token", tenant.getToken());                  // update client token
                update.set("timestamp", tenant.getTimestamp());
                UpdateResult result = mongoTemplate.updateFirst(query, update, TenantInfo.class);
                if (Objects.isNull(result))
                    log.warn("No documents updated");
                else {
                    System.out.println(result.getModifiedCount() + " document(s) updated..");
                    if (!tenant.getIdTenant().contains("RNExpo"))
                        notificationService.suscribeTokenFS(tenant.getToken(), "promociones");
                }
                return false;
            } else return true;
        }
    }

    @GetMapping("/testNtfFS")
    public ResponseEntity<Object> sendNotification() {
        try {
            notificationService.sendNotification();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // expo
    @PostMapping("/addXP")
    public ResponseEntity<Object> addTokenXP(@RequestBody TenantInfo tenantInfo) throws Exception {
        if (tenantInfo == null)
            return new ResponseEntity<>("datos requerido", HttpStatus.PRECONDITION_REQUIRED);
        else {
            try {
                if (manageAdd(tenantInfo))
                    iTenantRepository.save(tenantInfo);
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
            }
        }
    }

    // notificar nuevas promociones con array de emails con tokens mobile, si no existe no hace nada
    @PostMapping ("/sendDeals")
    public ResponseEntity<Object> sendDeals(@RequestBody RecipientsDTO recipients) throws Exception {
        if (!recipients.getRecipients().isEmpty()) {
            recipients.getRecipients().forEach(r -> {
                System.out.println(r);
                TenantInfo t = iTenantRepository.findById(r).orElse(null);
                if (Objects.nonNull(t)) {
                    String recipient = t.getToken(); // ExpoToken generado en mobile
                    String title = "UruBuy";
                    String message = "Ven a ver las últimas ofertas para navidad \uD83C\uDF85 \uD83C\uDF84";
                    Map<String, Object> data = new HashMap<>(); // set category to make different notifications
                    data.put("category", "promociones");

                    if (!PushClient.isExponentPushToken(recipient))
                        throw new Error("Token:" + recipient + " is not a valid token.");

                    ExpoPushMessage expoPushMessage = new ExpoPushMessage();
                    expoPushMessage.getTo().add(recipient);
                    expoPushMessage.setTitle(title);
                    expoPushMessage.setBody(message);
                    expoPushMessage.setData(data);

                    List<ExpoPushMessage> expoPushMessages = new ArrayList<>();
                    expoPushMessages.add(expoPushMessage);

                    PushClient client = null;
                    try {
                        client = new PushClient();
                    } catch (PushClientException e) {
                        throw new RuntimeException(e);
                    }
                    List<List<ExpoPushMessage>> chunks = client.chunkPushNotifications(expoPushMessages);

                    List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures = new ArrayList<>();

                    for (List<ExpoPushMessage> chunk : chunks) {
                        messageRepliesFutures.add(client.sendPushNotificationsAsync(chunk));
                    }

                    // Wait for each completable future to finish
                    List<ExpoPushTicket> allTickets = new ArrayList<>();
                    for (CompletableFuture<List<ExpoPushTicket>> messageReplyFuture : messageRepliesFutures) {
                        try {
                            allTickets.addAll(messageReplyFuture.get());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    // esto se queda viendo si llegó y nose creo q no hace falta
                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets = client.zipMessagesTickets(expoPushMessages, allTickets);

                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> okTicketMessages = client.filterAllSuccessfulMessages(zippedMessagesTickets);
                    String okTicketMessagesString = okTicketMessages.stream().map(
                            p -> "Title: " + p.message.getTitle() + ", Id:" + p.ticket.getId()
                    ).collect(Collectors.joining(","));
                    System.out.println(
                            "Recieved OK ticket for " +
                                    okTicketMessages.size() +
                                    " messages: " + okTicketMessagesString
                    );

                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> errorTicketMessages = client.filterAllMessagesWithError(zippedMessagesTickets);
                    String errorTicketMessagesString = errorTicketMessages.stream().map(
                            p -> "Title: " + p.message.getTitle() + ", Error: " + p.ticket.getDetails().getError()
                    ).collect(Collectors.joining(","));
                    System.out.println(
                            "Recieved ERROR ticket for " +
                                    errorTicketMessages.size() +
                                    " messages: " +
                                    errorTicketMessagesString
                    );
                }
            });
            return new ResponseEntity<>(HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // notify news!
    @PostMapping ("/sendNews")
    public ResponseEntity<Object> sendNews(@RequestBody RecipientsDTO recipients) throws Exception {   // lista de emails
        if (!recipients.getRecipients().isEmpty()) {
            recipients.getRecipients().forEach(r -> {
                System.out.println(r);
                TenantInfo t = iTenantRepository.findById(r).orElse(null);
                if (Objects.nonNull(t)) {
                    String recipient = t.getToken(); // ExpoToken generado en mobile
                    String title = "UruBuy";
                    String message = "Estos nuevos productos te van a encantar!";
                    Map<String, Object> data = new HashMap<>(); // set category to make different notifications
                    data.put("category", "nuevos");

                    if (!PushClient.isExponentPushToken(recipient))
                        throw new Error("Token:" + recipient + " is not a valid token.");

                    ExpoPushMessage expoPushMessage = new ExpoPushMessage();
                    expoPushMessage.getTo().add(recipient);
                    expoPushMessage.setTitle(title);
                    expoPushMessage.setBody(message);
                    expoPushMessage.setData(data);

                    List<ExpoPushMessage> expoPushMessages = new ArrayList<>();
                    expoPushMessages.add(expoPushMessage);

                    PushClient client = null;
                    try {
                        client = new PushClient();
                    } catch (PushClientException e) {
                        throw new RuntimeException(e);
                    }
                    List<List<ExpoPushMessage>> chunks = client.chunkPushNotifications(expoPushMessages);

                    List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures = new ArrayList<>();

                    for (List<ExpoPushMessage> chunk : chunks) {
                        messageRepliesFutures.add(client.sendPushNotificationsAsync(chunk));
                    }

                    // Wait for each completable future to finish
                    List<ExpoPushTicket> allTickets = new ArrayList<>();
                    for (CompletableFuture<List<ExpoPushTicket>> messageReplyFuture : messageRepliesFutures) {
                        try {
                            allTickets.addAll(messageReplyFuture.get());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    // esto se queda viendo si llegó y nose creo q no hace falta
                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets = client.zipMessagesTickets(expoPushMessages, allTickets);

                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> okTicketMessages = client.filterAllSuccessfulMessages(zippedMessagesTickets);
                    String okTicketMessagesString = okTicketMessages.stream().map(
                            p -> "Title: " + p.message.getTitle() + ", Id:" + p.ticket.getId()
                    ).collect(Collectors.joining(","));
                    System.out.println(
                            "Recieved OK ticket for " +
                                    okTicketMessages.size() +
                                    " messages: " + okTicketMessagesString
                    );

                    List<ExpoPushMessageTicketPair<ExpoPushMessage>> errorTicketMessages = client.filterAllMessagesWithError(zippedMessagesTickets);
                    String errorTicketMessagesString = errorTicketMessages.stream().map(
                            p -> "Title: " + p.message.getTitle() + ", Error: " + p.ticket.getDetails().getError()
                    ).collect(Collectors.joining(","));
                    System.out.println(
                            "Recieved ERROR ticket for " +
                                    errorTicketMessages.size() +
                                    " messages: " +
                                    errorTicketMessagesString
                    );
                }
            });
            return new ResponseEntity<>(HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
